### 1. What kinds of behaviour cause sexual harassment?

  - Sexual harassment is unwelcome sexual behaviour that’s offensive, humiliating or intimidating. It can be written, verbal or physical, and can happen in person or online.Anybody can experience sexual harassment, regardless of their gender. When it happens at work, school or uni, sexual harassment may amount to a form of discrimination.

##### - Kinds of behaviour cause sexual harassment
        1. touching, grabbing or making other physical contact with you without your consent.
        2. making comments to you that have a sexual meaning.
        3. asking you for sex or sexual favours.
        4. leering and staring at you.
        5. displaying rude and offensive material so that you or others can see it.
        6. making sexual gestures or suggestive body movements towards you.
        7. cracking sexual jokes and comments around or to you.
        8. questioning you about your sex life.
        9. insulting you with sexual comments.
        10. behaviour on a phone call that makes you feel uncomfortable.
        11. indecently exposing themselves to you.
        12. sexually assaulting you.


### 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- If you suspect you are being sexually harassed, you need to take steps to try to resolve the problem . 

##### - You should do the following:

  - **Talk to the harasser.** If you feel safe doing so, you should talk to the person harassing you about his offensive behavior. Some people do not realize that they are doing anything wrong, so talking about the problem could resolve it. You want to be as specific as possible about the offensive behavior and tell the person to stop doing it. If that does not resolve the problem, write him a memo detailing the offensive actions and ask that the behavior stop immediately. Keep a copy of the memo for your records.
  - **Complain to your supervisor.** Inform your supervisor of the offensive behavior—unless he is the harasser—and about the steps you have used to resolve the problem. If you do not feel safe talking directly to the person harassing you, start by discussing the problem with your supervisor or human resources department. Follow up by putting your complaint in writing and keeping a copy of it in case you later need it.
  - **Follow your employer’s internal complaint process.** Follow your employer’s internal complaint process. Some companies have a specific process for handling sexual harassment complaints that you will want to comply with. If you belong to a union, ask your union representative for help taking the next step in making a complaint. Again, you need to document everything in writing and keep a record of what you have done.
  - **Keep a journal.** Keep a journal at your home that documents all incidents of suspected sexual harassment and your attempts to resolve the problem. Be certain to note the date, time, and witnesses to each incident.
    Keep copies of anything offensive. You should retain copies or pictures of any offensive posters, notes, or pictures that relate to your claim of illegal harassment.
  - **Keep copies of your work records.** Save copies of all work-related documents, including your job evaluations and other memos or letters regarding the quality of your work. Your employer could defend against a claim of discrimination by claiming your job performance was unsatisfactory.
  - **Network with co–workers.** If possible, discuss your situation with other co-workers. You may discover witnesses, supporters who can later help you, or others who are the victim of discrimination.
  - **File an administrative complaint.** You should file a complaint with the Equal Employment Opportunity Commission (EEOC) and the FEHA. When you file with one agency, you can ask them to cross-file your complaint with the other agency. It is extremely important that you file your complaints because these are required before you can file a lawsuit. You must file your complaint with the EEOC within 300 days of the last act of discrimination and within one year of the last act of discrimination with the FEHA—or you waive your right to file a complaint.
  - **Contact an attorney.** You should contact an experienced employment discrimination attorney as soon as possible if your initial steps to stop the harassment do not resolve the problem. He can advise you on the best steps to take and can help you file your administrative complaints.
