# Learning Process

## 1. How to Learn Faster with the Feynman Technique

#### Q.1 What is the Feynman Technique? Paraphrase the video in your own words.

- The Feynman Technique is a four-step process to understand any topic. If you need to understand the topic, try to explain the topic to someone else in simple language. Explaining the concepts to someone else can also help us to clear the points of the concepts where we can be stuck.
- In this video, Pythagorean theorem example is used to explain about the video.

#### Q.2 What are the different ways to implement this technique in your learning process?

- Step 1 - Take a piece of paper and write the concept name on it.
- Step 2 - Explain/Teach the concept to someone else in plain & simple language.
- Step 3 - Identify the points of the concept, that you were stuck on explaining. Go back to the notes until you clear the point where were you stuck.
- Step 4 - Pinpoint the complicated terms & simply the points on yourself, which can be later easier to explain the concepts to others.

## 2. Learning How to Learn TED talk by Barbara Oakley

#### Q.1 Paraphrase the video in detail in your own words.

- This video is about effective learning. There were many techniques that were discussed in the video. Every technique was explained in detail with real-life examples. The speaker walked us through her journey from a soldier to a professor.  She picturized the problems that she faced and how she tackled them, she also said the techniques that she has today if these techniques were known to her earlier or she was taught with these techniques, she would have reached her goals easier & earlier.

Some of the techniques of learning are -
1. Focus mode
2. Defuse mode
3. Procrastination problem
4. Pomodoro technique

#### Q.2 What are some of the steps that you can take to improve your learning process? 

- Focus Mode - Draw your complete focus on the topic without getting distracted. While learning in focus mode, the topics makes a pattern inside the brain. 

- Diffuse mode - While learning a new topic, it goes as thoughts get collide with previous learnings, memories, and ideas. And when the thoughts do not find the match it leads to distraction & unable to focus.
To deal with the problems in difuse mode, a solution was suggested of relaxing & dragging your focus from difuse mode to focus mode.

- Procrastination problem - when we work on something & sometimes we get distracted as we start doing some other work for more time, than the important topic. To avoid the procrastination problem, there is the Pomodoro technique.

- Pomodoro technique -  Pomodoro technique is, in which we need to get a timer for focusing on a thought for at least 25 mins. when we are done with the work in 25 mins, get a small break to get relaxed. Simultaneously you are practicing your attention and relaxing.

## 3. Learn Anything in 20 hours

#### Q.1 Your key takeaways from the video? Paraphrase your understanding.

- This is a very nice video for those who are afraid of acquiring new skills or stop making efforts when they face little short backs. There are many takeaways from this video, one of which I want to mention is, he himself practices his technique of learning a new skill or acquiring a new skill in 20 hours before presenting it or writing about it. while presenting his skill on stage he was still in a learning phase this shows how confident he is about his technique & how efficiently his technique works. While playing the ukulele (small Hawaiian guitar) he mentioned that just now he finished his 20th hour of practicing this skill, I was impressed by his presenting skills, and kept the interaction interesting. 


#### Q.2 What are some of the steps that you can while approaching a new topic? 

- Deconstruct the skill - check your need, how much you need to acquire skill.
- Learn enough to self-correct - Do not depend on one resource always have options to compare.  
- Remove practice barriers -  Always concentrate on learning and stay away from distractions.
- practice at least 20 hours -  There is always room for improvement so practice as much as you can, at least 20 hours.

