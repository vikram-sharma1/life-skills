# Life Skills Topics - Load Balancer & Horizontal, Vertical Scaling

# Load Balancing


- Load balancing is the process of distributing network traffic across multiple servers. Load Balancer balances the load from the server & it equally distributes the load to backend servers, so the load should not affect backend nodes or the servers. Load balancer ensures that there is optimal performance of the application & none of the nodes gets overloaded.

    <img src="https://avinetworks.com/wp-content/uploads/2018/11/server-load-balancing-diagram.png" alt="Load Balancer Image">

### Types of load balancing

1. Load Balancing With Hardware

    - Hardware load balancing is a typical strategy for balancing the traffic. The dedicated hardware devices which are used to handle the network traffic and load balancing is called hardware load balancing. Majority of the dedicated hardware load balancers runs Linux. The main disadvantage of load balancing with hardware is that it’s very expensive.


2. Load Balancing With Software

    - The Software-based load balancing is exceptionally a very effective and reliable method for distributing load between servers. The software performs the balancing of requests usually on a Linux platform with a wide variety of algorithms for server allocation.

### Benifts of using the load balancer

    - Reduced downtime
    - Scalable
    - Redundancy
    - Flexibility
    - Efficiency

### Load balancing algorithm

    1. Round Robin Algorithm
    2. Least connections
    3. Weighted least connections
    4. Source IP hash
    5. URL hash
    6. The least response time
    7. The least bandwidth method
    8. The custom load method


### <a href="https://docs.oracle.com/en-us/iaas/Content/Balance/Troubleshooting/common_load_balancer_errors.htm">Common Load Balancer Errors</a> 
    
#### Server Errors (500-599)

    - 504, 502
    - Backend connection issue
    - Session persistence issue

    *For all other 5nn errors, the most likely cause will be issues with the backend server.

#### Client Errors (400-499)

    - 400, 404, 403

#### Health Check Errors

    - No healthy backends
    - Status code issues
    - Response match failed
    - Unreachable host
    - Health status issues
    - Connection issues
 
#### SSL Errors

    - SSL virtual listener issues
    - SSL handshake issues
    - Backend SSL handshake issues
    - SSL certificate issues
    - Client SSL certificate issues
    - SSL error causes backend health check failure
    - SSL host name verification fails

#### Client-side Errors

    - Client access denied
    - Client timeout issue


![](https://assets.digitalocean.com/articles/high_availability/ha-diagram-animated.gif)

<hr>
<hr>




# Scaling

- Scaling alters size of a system. In the scaling process, we either compress or expand the system to meet the expected needs. The scaling operation can be  achieved by adding resources to meet the smaller expectation in the current system, or by adding a new system in the existing one, or both. 


### Types of Scaling
    - Vertical Scaling
    - Horizontal Scaling


<img src="https://media.geeksforgeeks.org/wp-content/cdn-uploads/20210209202449/Scaling-Concept.png" alt="Scaling Image">


### Vertical Scaling

- When new **resources** are added in the existing system to meet the expectation, it is known as vertical scaling. Vertical Scaling are easy to implement as it expands the size of the existing system vertically.
- Vertical Scaling takes less time to be done.

Eg - MySQL, Amazon RDS

#### Advantages of vertical scaling

1. **Cost-effective -** Upgrading a pre-existing server costs less than purchasing a new one. Additionally, you are less likely to add new backup and virtualization software when scaling vertically. Maintenance costs may potentially remain the same too.
2. **Less complex process communication -** When a single node handles all the layers of your services, it will not have to synchronize and communicate with other machines to work. This may result in faster responses.
3. **Less complicated maintenance -** Not only is maintenance cheaper but it is less complex because of the number of nodes you will need to manage. 
4. **Less need for software changes -** You are less likely to change how the software on a server works or how it is implemented.         

#### Disadvantages of vertical scaling

1. **Higher possibility for downtime -** Unless you have a backup server that can handle operations and requests, you will need some considerable downtime to upgrade your machine. 
2. **Single point of failure -** Having all your operations on a single server increases the risk of losing all your data if a hardware or software failure was to occur. 
3. **Upgrade limitations -** There is a limitation to how much you can upgrade a machine. Every machine has its threshold for RAM, storage, and processing power.


### Horizontal Scaling

- When new **server racks** are added in the existing system to meet the higher expectation, it is known as horizontal scaling. Horizontal scaling are difficult to implement as it expands the size of the existing system horizontally.
- Horizontal Scaling takes more time to be done.

Eg - Cassandra, MongoDB, Google Cloud Spanner

#### Advantages of horizontal scaling

1. **Scaling is easier from a hardware perspective -** All horizontal scaling requires you to do is add additional machines to your current pool. It eliminates the need to analyze which system specifications you need to upgrade.
2. **Fewer periods of downtime -** Because you’re adding a machine, you don’t have to switch the old machine off while scaling. If done effectively, there may never be a need for downtime and clients are less likely to be impacted.
3. **Increased resilience and fault tolerance -** Relying on a single node for all your data and operations puts you at a high risk of losing it all when it fails. Distributing it among several nodes saves you from losing it all. 
4. **Increased performance -** If you are using horizontal scaling to manage your network traffic, it allows for more endpoints for connections, considering that the load will be delegated among multiple machines.     

#### Disadvantages of horizontal scaling

1. **Increased complexity of maintenance and operation -** Multiple servers are harder to maintain than a single server is. Additionally, you will need to add software for load balancing and possibly virtualization. Backing up your machines may also become a little more complex. You will need to ensure that nodes synchronize and communicate effectively. 
2. **Increased Initial costs -** Adding new servers is far more expensive than upgrading old ones.

### 5 Mistakes Made When Scaling a Database

    1. Allowing Redundancies
    2. Not Updating your Indexes 
    3. Bringing the Wrong Kind of Data together
    4. Not using Topology
    5. Not Knowing when they are Bad


![](https://www.esds.co.in/blog/wp-content/uploads/2021/03/ScalingupvsScalingout.gif)


<hr>
<hr>

## Resources

- <a href="https://en.wikipedia.org/wiki/Load_balancing_(computing)#Static_and_dynamic_algorithms">Load Balancer Wikipedia</a>
- <a href="https://lavellenetworks.com/blog/8-load-balancing-techniques-you-should-know/">Load Balancer</a>
- <a href="https://docs.oracle.com/en-us/iaas/Content/Balance/Troubleshooting/common_load_balancer_errors.htm">Common Load Balancer Errors</a>
- <a href="https://docs.oracle.com/en-us/iaas/Content/Balance/Reference/troubleshooting_load_balancer.htm">Troubleshooting Load Balancing</a>
- <a href="https://www.geeksforgeeks.org/horizontal-and-vertical-scaling-in-databases/">Vert, Hoz Scaling</a>
- <a href="https://www.section.io/blog/scaling-horizontally-vs-vertically/">Diff Vert, Hoz Scaling</a>
- <a href="http://highscalability.com/blog/2014/5/12/4-architecture-issues-when-scaling-web-applications-bottlene.html">More Information on Scaling</a>
- <a href="https://medium.com/geekculture/5-mistakes-made-when-scaling-a-database-b9e67dcbc0b8">More Information on, Mistakes while Scaling</a>




