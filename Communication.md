# Listening and Active Communication

## 1.Active Listening

#### Q.1 What are the steps/strategies to do Active Listening? 

- **Face the speaker and have eye contact :** Eye contact is an important part of face to face conversation. Too much eye contact can be intimidating, though, so adapt this to the situation you’re in. Try breaking eye contact every five seconds or so, or to show you’re listening attentively, look at one eye for five seconds, then another eye for five seconds, then switch to looking at their mouth. When you look away, looking to the side or up is better than looking down, which can seem like you want to close the conversation.

- **Don’t interrupt :** Being interrupted is frustrating for the other person – it gives the impression that you think you’re more important, or that you don’t have time for what they have to say. If you are naturally a quicker thinker or speaker, force yourself to slow down so that the other person can express themselves. Remember, a pause or a few seconds of silence doesn’t mean that you have to jump in. Letting the other person speak will make it easier for you to understand their message, too.

- **Stay focused :** If you're finding it difficult to focus on what someone is saying, try repeating their words in your head as they say them – this will reinforce what they’re saying and help you to concentrate. Try to shut out distractions like other conversations going on in the room.

- **Ask questions :** Asking relevant questions can show that you’ve been listening and help clarify what has been said. If you’re not sure if you’ve understood correctly, wait until the speaker pauses and then say something like “Did you mean that x…” Or “I’m not sure if I understood what you were saying about…”

- **Don’t start planning what to say next :** You can’t listen and prepare at the same time.

- **Don’t impose your opinions or solutions :** It’s not always easy, but lending a listening, supportive ear can be much more rewarding than telling someone what they should do. When a loved one has health problems is a time when they probably want to tell you how they’re feeling, and get things off their chest, rather than have lots of advice about what they should be doing. 


## 2.Reflective Listening

#### Q.1 According to Fisher's model, what are the key points of Reflective Listening?

- Reflective listening is a special type of listening that involves paying respectful attention to the content and feeling expressed in another persons’ communication. Reflective listening is hearing and understanding, and then letting the other know that he or she is being heard and understood. It requires responding actively to another while keeping your attention focused completely on the speaker. In reflective listening, you do not offer your perspective by carefully keep the focus on the other’s need or problem

## 3.Reflection

#### Q.1 What are the obstacles in your listening process?

- **Noise** Any external noise can be a barrier, like the sound of equipment running, phones ringing, or other people having conversations.
- **Visual distractions** Visual distractions can be as simple as the scene outside a window or the goings-on just beyond the glass walls of a nearby office.
- **Physical setting** An uncomfortable temperature, poor or nonexistent seating, bad odors, or distance between the listener and speaker can be an issue.
- **Objects** Items like pocket change, pens, and jewelry are often fidgeted with while listening.
- **The person speaking** The person listening may become distracted by the other person’s personal appearance, mannerisms, voice, or gestures.
- **Anxiety** Anxiety can take place from competing personal worries and concerns.
- **Self-centeredness** This causes the listener to focus on his or her own thoughts rather than the speaker’s words.
- **Mental laziness** Laziness creates an unwillingness to listen to complex or detailed information.
- **Boredom** Boredom stems from a lack of interest in the speaker’s subject matter.
- **Sense of superiority** This leads the listener to believe they have nothing to learn from the speaker.
- **Cognitive dissonance** The listener hears only what he or she expects or molds the speaker’s message to conform with their own beliefs.
- **Impatience** A listener can become impatient with a speaker who talks slowly or draws out the message.

#### Q.2 What can you do to improve your listening?

1. Consider eye contact.
2. Be alert, but not intense.
3. Pay attention to nonverbal signs, such as body language and tone.
4. Make a mental image of what the speaker is saying.
5. Empathise with the speaker.
6. Provide feedback.
7. Keep an open mind.

## 4. Types of Communication

#### Q.1 When do you switch to Passive communication style in your day to day life?
- Passive communication is a style in which individuals have developed a pattern of avoiding expressing their opinions or feelings, protecting their rights, and identifying and meeting their needs. As a result, passive individuals do not respond overtly to hurtful or anger-inducing situations.
- Passive communication style can be switched when some of the points occured :

1. People may disregard your needs and opinions.
2. You’re likely to be passed over for projects and promotions and miss out on interesting opportunities. 
3. You become complicit in poor choices because you don’t express your discomfort.
4. You may feel angry,  resentful or stressed by other people’s inconsideration. 
5. You may bottle up your feelings claiming to be a peace-maker. But, in reality,  you feel powerless and hopeless.


#### Q.2 When do you switch into Aggressive communication styles in your day to day life?

- Aggressiveness is a mode of communication and behavior where one expresses their feelings, needs, and rights without regard or respect for the needs, rights, and feelings of others.
- Aggressive communication can be switched when some of the points occured :

1. We avoid coming to the point. We tend to beat around the bush and drop hints, hoping that others will understand.
2. We speak softly and apologetically. We aren’t confident of our thoughts, opinions and actions, and don’t want to be seen as harsh or opinionated.
3. Our body language is passive; we slouch and avoid eye-contact. Poor posture and hesitant gestures can reflect insecurity and lack of confidence.


#### Q.3 When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- Passive-Aggressive communication style appears to be passive, but behind the scenes, they act out angrily in indirect ways. People who develop this communication style tend also to have a fear of confrontation, indirectly dealing with difficult situations, which may look like they’re “sneaky” to others. Instead of directly asking for their needs to be met, a passive-aggressive communication style will show their negative emotions in subtle and oftentimes harmful behaviors.
- Passive Aggressive communication can be switched when some of the points occured :


1. Sarcasm
2. Subtle sabotage
3. Pretending to be cooperative while subconsciously doing tasks incorrectly
4. Mumbling to themselves instead of confronting the person
5. Emotional withdrawal
6. Talking behind someone’s back
7. Quitting unexpectedly with no explanation


#### Q.4 How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

1. **Believe in yourself.** Low self-esteem can prevent you from telling others what you want. When you begin to really value your self-worth, you will have an easier time communicating your needs. Worrying about the opinions of others can stand in the way of being honest with yourself and others. Being assertive is a clear sign of self-respect. 
2. **Start small.** If you’re having a hard time finding that assertive voice, begin with small things. Perhaps you still hear a rattling after you pick your car up from the shop, but you’re afraid to question your mechanic’s work. Say something. Begin to practice daily rituals of putting yourself first. 
3. **Be simple and direct.** Don’t leave room for misinterpretation. Assertiveness is effective because it is straight to the point. Tell the other person how you’re feeling using the pronoun “I”—as in “I feel” or “I think.” This is a confident approach. Beginning with “you” presumes to know what the other side is feeling and comes across as a more aggressive communication style. 
4. **Keep it positive.** No one likes difficult conversations and as a result, things go unresolved which leads to stress and complicated relationships. One way to stop procrastinating and deal with situations that require you to be assertive is to approach them with positive emotions. If you communicate with an upbeat tone, the other person will often let down their guard and respond accordingly. 
5. **Learn how to say “no.”** Often, people are reluctant to say “no” to others in order to be people-pleasers, even if saying “yes” creates an inconvenience for them. Whether it’s taking on a colleague’s extra work or watching a friend’s dog, helping others makes people feel good. But it’s important to recognize when your life needs to take priority over helping someone out. If you have a lot on your plate already and you can’t take on more at the moment, simply say no. 



