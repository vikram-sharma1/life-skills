# Grit and Growth Mindset

## 1. Grit

#### Q.1 Paraphrase (summarize) the video in a few lines. Use your own words.

- Research of many years has shown that to last in your learning process one needs to be gritty it doesn't matter how intelligent he is and how high IQ he has. Also, his potential will not help until and unless he is willing to put that potential to work. To be gritty it only takes a growth mindset to develop this ability. Comparatively, a gritty person lasts long and performs better than an intelligent person.

#### Q.2 What are your key takeaways from the video to take action on?

- This video broadly explains the need for willpower to achieve a growth mindset and ultimately to get your goals. It is we who can get our goals with our willingness to do so, factors like intelligence do not decide one's success story.

## 2. Introduction to Growth Mindset

#### Q.1 Paraphrase (summarize) the video in a few lines. Use your own words.

- It is brilliantly explained how a mindset for growth can lead you to a successful life. it only takes your mindset to change to lead a better life. One should put the effort into learning, always accept challenges and give 100% to achieve them, accept failures and convert them into your success, and take feedback and work on them, this helps you to embrace a growth mindset.

#### Q.2 What are your key takeaways from the video to take action on?

- One can follow two types of mindset which defines their personality, these are fixed mindset & growth mindset and it totally depends on us which mindset we embrace. By introspecting according to the following points one can change his mindset or get to know his shortfalls -

    1. Never back down from making efforts for learning.
    2. Accept challenges.
    3. Embrace failures & work on them.
    4. Always work on feedback and improve

## 3. Understanding Internal Locus of Control

#### Q.1 What is the Internal Locus of Control? What is the key point in the video?

- The Internal Locus of Control is the ability to self-motivate yourself to achieve any goal. It is the driving force toward success. It differentiates doers from excuse-makers. It encourages you to look for the reasons to complete your task instead of looking for excuses and quitting. 

- There are many key points mentioned in the video, the one which I like the most is that one should always remain positive & self-motivated. Always keep yourself in the doer's category & the barrier between you and your success is emotional.

## 4. How to build a Growth Mindset

#### Q.1 Paraphrase (summarize) the video in a few lines. Use your own words.

- The path to a growth mindset was broken down and explained in this video, some steps involved were also mentioned and how to overcome our failures was also covered. 

#### Q.2 What are your key takeaways from the video to take action on?

- In order to achieve a growth mindset, one should be giving his 100% focus toward their goal and always be ready to take up any challenges.
    1. Always believe in yourself that you can achieve whatever you desire.
    2. Always questions your assumptions and never let your present knowledge narrow down your vision for the future.
    3. Never give up and question your potential as you can do better.
    4. Plan for your future, architect a path towards your goal and make it possible.

## 5. Mindset - A MountBlue Warrior Reference Manual

- I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.
- I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.
